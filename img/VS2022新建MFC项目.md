## 计算机图形学孔令德VS2022新建MFC项目基本操作

#### VS没有MFC组件

如果你的vs2022在安装的时候没有下载mfc组件的话，需要自行下载

如果下载了，如下图

![image-20221004153558810](image-20221004153558810.png)

没有下载，则点击上图中的安装多个工具和功能

![image-20221004153723135](image-20221004153723135.png)

选择单个组件，搜索MFC

![image-20221004153937332](image-20221004153937332.png)

选中下载即可

#### VS2022新建MFC项目

打开VS2022，选择创建新项目

<img src="image-20221004154050930.png" alt="image-20221004154050930" style="zoom:67%;" />

选择MFC应用，然后下一步

![image-20221004154132931](image-20221004154132931.png)

项目名称命名为test1（自己随便取名）

![image-20221004154145719](image-20221004154145719.png)

然后应用程序类型选择单个文档，然后点完成

![image-20221004154334441](image-20221004154334441.png)

#### VS2022绘制金刚石图案

结果如下图

![image-20221004154705419](image-20221004154705419.png)

右键点击项目test1，选择添加，选择类

![image-20221004154828307](image-20221004154828307.png)

类名填写CP2

![image-20221004154915257](image-20221004154915257.png)

![image-20221004154930266](image-20221004154930266.png)

为保证命名的统一性，把.h文件和.cpp文件的c删掉

![image-20221004155022777](image-20221004155022777.png)

如图，然后确定

同理，添加Cdiamood类

![image-20221004155106054](image-20221004155106054.png)

##### 绘制金刚石详细代码参考

P2.h文件代码如下

```c++
#pragma once
class CP2
{
public:
	CP2(void);
	CP2(double x, double y);
	virtual ~CP2(void);
public:
	double x, y;
};

```

P2.cpp文件代码如下

```c++
#include "pch.h"
#include "P2.h"

CP2::CP2(void) {
	x = 0.0;
	y = 0.0;
}

CP2::CP2(double x, double y)
{
	this->x = x;
	this->y = y;
}

CP2::~CP2(void)
{

}
```

Diamood.h文件代码如下

```c++
#pragma once
#include "P2.h"
class CDiamood
{
public:
	CDiamood(void);
	virtual ~CDiamood(void);
	void SetParameter(int n, double r, CP2 Pt);//设置金刚石的参数
	void ReadVertex(void);//用于读入各个等分点的坐标
	void Draw(CDC* pDC);//用于绘制金刚石图案
private:
	int n;//等分点的个数
	double r;//圆的半径
	CP2 Pt;//金刚石中心点
	CP2* P;//用于存储各个等分点
};

```

Diamood.cpp文件代码如下

```c++
#include "pch.h"
#include "Diamood.h"

#define PI 3.1415926//宏定义π的值
#define ROUND(d) int(d+0.5) //进行四舍五入

CDiamood::CDiamood(void)
{
	n = 0;
	r = 0;
	P = NULL;
}
CDiamood::~CDiamood(void)
{
	delete[]P;//撤销申请的动态数组的内存空间
	P = NULL;
}

void CDiamood::SetParameter(int n, double r, CP2 Pt)
{
	this->n = n;
	this->r = r;
	this->Pt = Pt;
	P = new CP2[n];//动态数组存储等分点
}

void CDiamood::ReadVertex(void)
{
	double Theta = 2 * PI / n;
	for (int i = 0; i < n; i++)
	{
		P[i].x = r * cos(i * Theta);
		P[i].y = r * sin(i * Theta);
	}
}

void CDiamood::Draw(CDC* pDC)
{
	for (int i = 0; i <=n-2; i++)//直线连接等分点
	{
		for (int j = i + 1; j <= n - 1; j++)
		{
			pDC->MoveTo(ROUND(Pt.x + P[i].x), ROUND(Pt.y + P[i].y));
			pDC->LineTo(ROUND(Pt.x + P[j].x), ROUND(Pt.y + P[j].y));
		}
	}
}
```

test1View.h文件添加部分代码，头文件添加`#include "Diamood.h"`

```c++

// 操作
public:
	void DrawObject(CDC* pDC);//绘制图形

// 重写
```

```C++
protected:
	CDiamood diamood;
// 生成的消息映射函数
```

test1View.cpp文件添加部分代码

OnDrew函数

```c++
void Ctest1View::OnDraw(CDC* pDC)
{
	Ctest1Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	//自定义坐标系
	CRect rect;
	GetClientRect(&rect);
	pDC->SetMapMode(MM_ANISOTROPIC);//设置映射模式
	//改变视口坐标系
	pDC->SetWindowExt(rect.Width(), rect.Height());//设置窗口
	pDC->SetViewportExt(rect.Width(), -rect.Height());//设置视区:x轴水平向右，y轴垂直向上
	//客户区中心为坐标系原点，rect.Width()/2,rect.Height()/2坐标是设备坐标系的。
	pDC->SetViewportOrg(rect.Width() / 2, rect.Height() / 2);
	rect.OffsetRect(-rect.Width() / 2, -rect.Height() / 2);
	DrawObject(pDC);
	// TODO: 在此处为本机数据添加绘制代码
}
```

DrawObject函数

```c++
void Ctest1View::DrawObject(CDC* pDC)
{
	diamood.Draw(pDC);
}
```

